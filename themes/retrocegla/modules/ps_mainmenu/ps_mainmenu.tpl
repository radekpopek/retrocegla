{assign var=_counter value=0}
{function name="menu" nodes=[] depth=0 parent=null}
    {if $nodes|count}
      <ul class="top-menu" {if $depth == 0}id="top-menu"{/if} data-depth="{$depth}">
        {foreach from=$nodes item=node}
            <li class="{if $_counter == 0} hidden-sm-down {/if}{$node.type}{if $node.current} current {/if}{if $node.children and $depth ===0 } dropdown {/if}" id="{$node.page_identifier}">
            {assign var=_counter value=$_counter+1}
              <a
                class="{if $node.children and $depth ===0 } dropdown-toggle {/if}{if $depth >= 0}dropdown-item{/if}{if $depth === 1} dropdown-menu{/if}"
                href="
                {if $node.label|lower == 'gwarancja'}
                    #gwarancja
                {elseif $node.label|lower == 'produkty'}
                    /10-plytki-z-lica/
                {else}
                    {$node.url}
                {/if}
                "
                data-depth="{$depth}"
                {if $node.children and $depth ===0 } data-toggle="dropdown" {/if}
                {if $node.open_in_new_window} target="_blank" {/if}
              >
                {if $node.children|count}
                  {* Cannot use page identifier as we can have the same page several times *}
                  {assign var=_expand_id value=10|mt_rand:100000}
                  <span class="float-xs-right hidden-md-up">
                    <span data-target="#top_sub_menu_{$_expand_id}" data-toggle="collapse" class="navbar-toggler collapse-icons">
                      <i class="material-icons add">&#xE313;</i>
                      <i class="material-icons remove">&#xE316;</i>
                    </span>
                  </span>
                {/if}
                {$node.label}
              </a>
              {if $node.children|count}
              <div {if $depth === 0} class="popover sub-menu js-sub-menu collapse"{else} class="collapse"{/if} id="top_sub_menu_{$_expand_id}">
                {menu nodes=$node.children depth=$node.depth parent=$node}
              </div>
              {/if}
            </li>
        {/foreach}
        {if $depth == 0}
            <li class="link" id="lnk-kontakt">
                <a class="dropdown-item" href="\kontakt" data-depth="0">
                    Kontakt
                </a>
            </li>
        {/if}
      </ul>
    {/if}
{/function}

<nav class="menu js-top-menu position-static hidden-sm-down" id="_desktop_top_menu">
    {menu nodes=$menu.children}
    <div class="clearfix"></div>
</nav>
