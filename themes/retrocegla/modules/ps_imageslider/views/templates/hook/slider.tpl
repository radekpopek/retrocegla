{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}

{if $homeslider.slides}
  <div id="retro-banners" data-wrap="{(string)$homeslider.wrap}">
    {foreach from=$homeslider.slides item=slide name='homeslider'}
        {if $smarty.foreach.homeslider.iteration == 4}
            <div id="aditional-banners" class="collapse">
        {/if}
        <div class="baner {if $smarty.foreach.homeslider.iteration == 1} big-baner{/if}">
            <a href="{$slide.url}">
                <figure>
                    <img src="{$slide.image_url}" alt="{$slide.legend|escape}">
                </figure>
            </a>
        </div>
        {if $smarty.foreach.homeslider.iteration == 1}
            <div class="some-another-menu">
                <ul>
                    <li class="hidden-md-up"><img class="swipe" src="{$urls.img_url}swipe.svg" alt="swipe"/></li>
                    <li><a href="/probki/40-oferta-demo.html">Zamów próbki cegieł</a></li>
                    <li><a href="/content/6-dostawa">Dostawa od 25 zł</a></li>
                    <li><a href="#gwarancja">5 lat gwarancji</a></li>
                    <li><a href="#realization">Realizacja zamówienia</a></li>
                </ul>
            </div>
        {/if}
        {if $smarty.foreach.homeslider.iteration == 5}
            </div>
        {/if}
    {/foreach}
      <div class="clearfix"></div>
  </div>
{/if}
