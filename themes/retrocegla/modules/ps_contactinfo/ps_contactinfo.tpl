{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}

<div class="block-contact col-lg-3 col-md-6 links wrapper">
    <h4 class="text-uppercase block-contact-title">{l s='Store information' d='Shop.Theme.Global'}</h4>
      <div class="shop-address">
        {$contact_infos.company nofilter}<br />
        {$contact_infos.address.address1 nofilter}, {$contact_infos.address.city nofilter}
      </div>
      <h4 class="text-uppercase block-contact-title">Kontakt</h4>
      <ul>
      {if $contact_infos.phone}
        {* [1][/1] is for a HTML tag. *}
        {l s='[1]telefon: %phone%[/1]'
          sprintf=[
          '[1]' => '<li><a href="tel:'|cat:$contact_infos.phone|cat:'">',
          '[/1]' => '</a></li>',
          '%phone%' => $contact_infos.phone
          ]
          d='Shop.Theme.Global'
        }
      {/if}
      {if $contact_infos.fax}
        {* [1][/1] is for a HTML tag. *}
        {l
          s='Fax: [1]%fax%[/1]'
          sprintf=[
            '[1]' => '<li>',
            '[/1]' => '</li>',
            '%fax%' => $contact_infos.fax
          ]
          d='Shop.Theme.Global'
        }
      {/if}
      {if $contact_infos.email}
        {* [1][/1] is for a HTML tag. *}
        {l
          s='[1]e-mail: %email%[/1]'
          sprintf=[
            '[1]' => '<li><a href="mailto:'|cat:$contact_infos.email|cat:'" >',
            '[/1]' => '</a></li>',
            '%email%' => $contact_infos.email
          ]
          d='Shop.Theme.Global'
        }
      {/if}
      </ul>
  </div>
</div>
