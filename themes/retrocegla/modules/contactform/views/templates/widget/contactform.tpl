{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<div class="row">
  <div class="col-md-12">
    <h1>Kontakt</h1>
  </div>
</div>
  <div class="row">
  <div class="col-lg-6">
  <section class="contact-form">
    <form action="{$urls.pages.contact}" method="post" {if $contact.allow_file_upload}enctype="multipart/form-data"{/if}>
      <h2>Wyślij wiadomość</h2>
      <p class="star">Pola oznaczone gwiazdką są obowiązkowe</p>
      {if $notifications}
        <div class="col-xs-12 alert {if $notifications.nw_error}alert-danger{else}alert-success{/if}">
          <ul>
            {foreach $notifications.messages as $notif}
              <li>{$notif}</li>
            {/foreach}
          </ul>
        </div>
      {/if}

      {if !$notifications || $notifications.nw_error}
        <section class="form-fields">
          <div class="form-group row">
            <div class="col-md-11">
              <input
                      type="text"
                      class="form-control subject required"
                      name="subject"
                      placeholder="Temat wiadomości *"
                      value="{$contact.subject}"
                      required
              />
            </div>
          </div>

          <div class="form-group row">
            <div class="col-md-11">
              <input
                      class="form-control"
                      name="order"
                      type="text"
                      placeholder="{l s='order_number' d='Shop.Forms.Help'}"
              >
            </div>
          </div>

          <div class="form-group row">
            <div class="col-md-11">
              <input
                      class="form-control"
                      name="name_surname"
                      type="text"
                      placeholder="{l s='name_surname' d='Shop.Forms.Help'} *"
                      value="{$contact.name_surname}"
                      required
              >
            </div>
          </div>

          <div class="form-group row">
            <div class="col-md-11">
              <input
                      class="form-control"
                      name="phone"
                      type="text"
                      placeholder="{l s='phone' d='Shop.Forms.Help'}"
                      value="{$contact.phone}"
                      required
              >
            </div>
          </div>

          <div class="form-group row">
            <div class="col-md-11">
              <input
                      class="form-control"
                      name="from"
                      type="email"
                      value="{$contact.email}"
                      placeholder="{l s='your@email.com' d='Shop.Forms.Help'} *"
                      required
              >
            </div>
          </div>

          {if $contact.allow_file_upload}
            <div class="form-group row">
              <div class="col-md-11">
                <input type="file" name="fileUpload" class="filestyle" data-buttonText="{l s='Choose file' d='Shop.Theme.Actions'}">
              </div>
            </div>
          {/if}

          <div class="form-group row">
            <div class="col-md-11">
              <textarea
                class="form-control"
                name="message"
                placeholder="{l s='How can we help?' d='Shop.Forms.Help'}"
                rows="6"
              >{if $contact.message}{$contact.message}{/if}</textarea>
            </div>
          </div>

        </section>
        <div class="g-recaptcha" data-sitekey="6LfZ9n8UAAAAAD35ymHJ9UL7IwbYOKuVNRGDgCOd"></div>
        <footer class="form-footer text-sm-left margin-top-20">
          <style>
            input[name=url] {
              display: none !important;
            }
          </style>
          <input type="text" name="url" value=""/>
          <input type="hidden" name="token" value="{$token}" />
          <input class="btn btn-send" type="submit" name="submitMessage" value="{l s='Send a message' d='Shop.Theme.Actions'}">
        </footer>
      {/if}

    </form>
  </section>
  </div>
  <div class="col-lg-6 right-side">
    <h2>Czeladź</h2>
    <p>tel. 786-238-248 (obsługa klienta)</p>
    <p>tel. 577-181-311 (obsługa klienta / aranżacje wnętrz)</p>
    <p>tel. 787-656-756 (klienci hurtowi / zarządzanie)</p>
    <p>telefon czynny w dni robocze między 9:00 a 17:00</p>
    <p>w sobotę, telefon czynny między 9:00 a 14:00</p>
    <p>e-mail: <a class="font600" href="mailto:{$shop.email}">{$shop.email}</a></p>
    <p>W przypadku odbioru osobistego prosimy o wcześniejsze umówienie się.</p>
    <p>Retro Holding Sp. z o.o. <br />
      ul. Wojkowicka 14a, 41-250 Czeladź</p>
    <h2 class="margin-top-20">Warszawa - Domoteka</h2>
    <p>tel. 693-494-780</p>
    <p>e-mail: <a class="font600" href="mailto:{$shop.email}">{$shop.email}</a></p>
    <p>Pon. -Sob: 10:00 - 21:00</p>
    <p>Niedz. (handlowa): 10:00 - 19:00</p>
    <p>Retro Holding Sp. z o.o. <br />
      ul. Malborska 41, 03-286 Warszawa</p>
    <p class="light-gray">Sklep internetowy retro-cegly.pl należy do: Retro Holding Sp. z o.o.,<br />
      ul. Wojkowicka 14a, 41-250 Czeladź,<br />
      NIP: 6252453201, KRS: 0000542271, REGON: 360724644,<br />
      kapitał zakładowy: 100 000 pln.</p>
  </div>
</div>