{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<section class="featured-products clearfix">
  <h1 class="h1 products-section-title text-uppercase">
    {l s='Popular Products' d='Shop.Theme.Catalog'}
  </h1>
  <div class="products">
    {foreach from=$products item="product"}
      {include file="catalog/_partials/miniatures/product.tpl" product=$product}
    {/foreach}
  </div>
  <div id="realization" class="realization">
    <div class="row">
      <div class="col-md-12">
        <h3 class="orange">Zobacz jak to wygląda w praktyce</h3>
        <h3>Realizujemy zamówienie w czterech prostych krokach</h3>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-3 col-md-6">
        <div class="number">
          <p>1</p>
        </div>
        <div class="title">
          <p>Zamówienie</p>
        </div>
        <div style="max-width: 202px;" class="text">
          <p>Po dokonaniu płatności, złożone zamówienie od razu trafia do realizacji.</p>
        </div>
        <div class="image">
          <img src="{$urls.img_url}2.svg" style="max-width: 96px" alt="Zamówienie" />
        </div>
      </div>
      <div class="col-lg-3 col-md-6">
        <div class="number">
          <p>2</p>
        </div>
        <div class="title">
          <p>Magazyn</p>
        </div>
        <div class="text">
          <p>Jeżeli zamówione produkty znajduja się na magazynie, realizacja zamówienia zwykle zajmuje do 3 dni.</p>
        </div>
        <div class="image">
          <img src="{$urls.img_url}1.svg" style="max-width: 176px" alt="Magazyn" />
        </div>
      </div>
      <div class="col-lg-3 col-md-6">
        <div class="number">
          <p>3</p>
        </div>
        <div class="title">
          <p>Realizacja</p>
        </div>
        <div style="max-width: 232px;" class="text">
          <p>Oryginalna RetroCegła cieszy się olbrzymią popularnością wśród klientów, dlatego zdarza się, że musimy wyprodukować płytki specjalnie dla Ciebie.</p>
        </div>
        <div class="image">
          <img src="{$urls.img_url}4.svg" style="max-width: 139px" alt="Magazyn" />
        </div>
      </div>
      <div class="col-lg-3 col-md-6">
        <div class="number">
          <p>4</p>
        </div>
        <div class="title">
          <p>Wysyłka</p>
        </div>
        <div class="text">
          <p>Staranna selekcja cegieł i ich profesjonalna obróbka, zajmuje kilka dni więcej. Niemniej jesteśmy profesjonalną firmą, która realizuje zamówienie w terminie.</p>
        </div>
        <div class="image">
          <img src="{$urls.img_url}4 v2.svg" style="max-width: 237px" alt="Wysyłka" />
        </div>
      </div>
    </div>
  </div>
</section>
