function dodaj_sciane(){	
	var id = wyznacz_id() + 1;
	var row = document.createElement('div');
	row.className='row';
			
	var wysokosc = document.createElement('input');
	var wysokosc_label = document.createElement('label');
	var wysokosc_text = document.createTextNode('wysokość');
	wysokosc_label.appendChild(wysokosc_text);
	wysokosc.setAttribute('type','text');
	wysokosc.setAttribute('name','wysokosc'+id);			
			
	var szerokosc = document.createElement('input');
	var szerokosc_label = document.createElement('label');
	var szerokosc_text = document.createTextNode('szerokość');
	szerokosc_label.appendChild(szerokosc_text);
	szerokosc.setAttribute('type','text');
	szerokosc.setAttribute('name','szerokosc'+id);
			
	var narozniki = document.createElement('input');
	var narozniki_label = document.createElement('label');
	var narozniki_text = document.createTextNode('uwzględnij narożniki');
	narozniki.setAttribute('type','checkbox');
	narozniki.setAttribute('id','naroznik'+id);
	narozniki.className = 'checbox';

	var checkmark = document.createElement('span');
	checkmark.className = 'checkmark';
	/* stworzenie klasy stylu dla naroznikow */
	var narozniki_style = document.createElement('div');
	narozniki_style.setAttribute('class','narozniki_style');
			
	var sciany = document.getElementById("sciany");			
			
	var etykieta = document.createElement('p');
	etykieta.innerHTML = "Ściana #"+id;
	etykieta.className = 'wall';
	var trash = document.createElement('span');
	var text = document.createElement('p');
	text.className = 'center';
	text.innerHTML = 'zaznacz(opcjonalnie)';
	trash.className = 'thrash';
	etykieta.appendChild(trash);
	row.appendChild(etykieta);
	row.appendChild(szerokosc_label);
	row.appendChild(szerokosc);
	row.appendChild(wysokosc_label);
	row.appendChild(wysokosc);
	row.appendChild(text);
	narozniki_label.appendChild(narozniki_text);
	narozniki_label.appendChild(narozniki);
	narozniki_label.appendChild(checkmark);
	narozniki_style.appendChild(narozniki_label);
	row.appendChild(narozniki_style);
	sciany.appendChild(row);		
}
function wyznacz_id(){
	var calosc = document.getElementById('sciany');
	return calosc.childElementCount;
}		
function round_up( value, precision ) { 
	var pow = Math.pow ( 10, precision ); 
	return ( Math.ceil ( pow * value ) + Math.ceil ( pow * value - Math.ceil ( pow * value ) ) ) / pow; 
}		
function convert(number){
	if(number) {
        number = number.replace(",", ".");
        if (isNaN(number)) {
            return 0;
        }
        else {
            return number;
        }
    }
    return 0;
}		
function oblicz(){
	var sciana_cala = 0;
	var ilosc_scian = wyznacz_id();
	var ilosc_naroznikow = 0;
	var i;
	for(i = 1; i <= ilosc_scian; i++){
		var sciana = 0;
		var wysokosc = convert(jQuery("input[name=wysokosc"+i+"]").val());
		var szerokosc = convert(jQuery("input[name=szerokosc"+i+"]").val());
		if(document.getElementById("naroznik"+i).checked==true){
			wybrane_plytki = jQuery('.chaosen').val();
			narozniki = round_up(wysokosc*13,0);
			ilosc_naroznikow = ilosc_naroznikow + narozniki;
		}	
		sciana = wysokosc * szerokosc;			
		sciana_cala = sciana_cala + sciana;
	}	
	if(document.getElementById("waste").checked==true){
		sciana_cala = round_up(sciana_cala,2);
		sciana_cala = round_up(1.1 * sciana_cala,1);
		ilosc_naroznikow = round_up(1.1 * ilosc_naroznikow,0);
	}					
	var wynik = (round_up(sciana_cala,2));
	var ilosc_paczek = round_up(wynik/0.5,0);
	document.getElementById("ilosc_lica").innerHTML = (ilosc_paczek/2) + " m<sup>2</sup>";
	jQuery('#ilosc_grunt .number').html(round_up(wynik/25,0));
	jQuery('#ilosc_impregnat .number').html(round_up(wynik/7,0));
    jQuery('#ilosc_klej .number').html(round_up(wynik/4,0));
    jQuery('#ilosc_fuga .number').html(round_up(wynik/2.5,0));
    document.getElementById("ilosc_naroznikow").innerHTML = (ilosc_naroznikow) + " szt";
};
jQuery(document).ready(function(){

	jQuery('#dodaj_sciane').click(function(){
		dodaj_sciane();
	});
	//usuwanie scian
	jQuery("#sciany").on("click",".thrash", function(){
		if(wyznacz_id()>1){
			jQuery(this).parent().parent().remove();
		}
	});

	jQuery('#oblicz').click(function(){
		oblicz();
	});

	jQuery('.bxslider button').click(function(){
		jQuery('.bxslider .chaosen').removeClass('chaosen');
		jQuery(this).addClass('chaosen');
	});
}); 