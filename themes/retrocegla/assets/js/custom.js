$(document).ready(function () {
    $('#product-mobile').click(function(){
        if($('#product-menu').hasClass('show')){
            $('#product-menu').removeClass('show');
            $('#product-mobile i').html('expand_more');
        }
        else{
            $('#product-menu').addClass('show');
            $('#product-mobile i').html('expand_less');
        }
    });
   $('#promocje').click(function () {
       if($('#banners').hasClass('full')){
           $('#banners').removeClass('full');
           $('#promocje p').html('Pokaż więcej promocji <i class="material-icons">expand_more</i>');
       }
       else{
           $('#banners').addClass('full');
           $('#promocje p').html('Zwiń promocje <br /><i class="material-icons">expand_less</i>');
       }


   });
   $('.show-more').click(function(){
       if($(this).children('i').text()=='expand_more'){
           $(this).children('i').text('expand_less');
           $(this).children('p').text('Zwiń promocje');
       }
       else{
           $(this).children('i').text('expand_more');
           $(this).children('p').text('Pokaż więcej promocji');
       }
   });
   $('footer .show-more').click(function(){
      if($(this).text()=="Pokaż więcej"){
          $(this).text("Ukryj");
      }
      else{
          $(this).text("Pokaż więcej");
      }
   });

   $('#cms-page-8 a').click(function(){
        $('html,body').animate({
            scrollTop: $('#gwarancja').offset().top
        }, "slow");
   });
   $('#do_gory').click(function(){
       $('html,body').animate({
           scrollTop: $('#kalkulator-plytek').offset().top
       }, "slow");
   });
   $('.bxslider').bxSlider({
       slideWidth : 276,
       maxSlides: 4,
       moveSlides: 1,
       pager: false,
       slideMargin: 12,
       controls: true,
       nextText: '',
       prevText: '',
       infiniteLoop: false,
       hideControlOnEnd: true,
   });
    $('#show_email').click(function(){
        $('#mobile-phone').animate({
            height: 0,
        }, 400);
        $('#mobile-email').animate({
            height: "109px",
        }, 400);
    });
    $('#show_phone').click(function(){
        $('#mobile-email').animate({
            height: 0,
        }, 400);
        $('#mobile-phone').animate({
            height: "109px",
        }, 400);
    });
    $('#hide-phone').click(function(){
        $('#mobile-phone').animate({
            height: 0,
        }, 400);
    });
    $('#hide-email').click(function(){
        $('#mobile-email').animate({
            height: 0,
        }, 400);
    });
    var pathname = window.location.pathname;

    if (pathname.match(/\/content/) || pathname.match(/\/kontakt/)){
        var headerheight = $('.header-nav').height() + $('.header-top').height();
        var scrollvalue = $('#wrapper').offset().top - headerheight;
        $("html,body").animate({scrollTop: scrollvalue},100);
    }else if(pathname !== '/'){
        $("html,body").animate({scrollTop: $('#wrapper').offset().top},100);
    }

    $(window).scroll(function () {
        var path = window.location.pathname;
        if(path.match(/\/content/) || path.match(/\/kontakt/)) {
            var scroll = $(window).scrollTop();
            var floatnav = $('.float-nav');
            if(scroll > 189){
                floatnav.css('position','fixed');
                $('#banners').css('margin-top','189px');
                floatnav.css('top',0);
                $('.logo').css({
                    'width' : '44%',
                    'max-height' : '44%',
                });
                $('#header .header-nav .search-widget').css('margin-top', 0);
                $('#header .header-nav .right-nav').css('margin-top','4px');
                $('#_desktop_user_info .login').css('width', '32px');
                $('#_desktop_cart .shopping-cart').css({
                    'width'  : '35px',
                    'hegiht' : '35px',
                });
                $('.header-contact .email').css('width', '39px');
                $('.header-contact .phone').css('width', '37px');
                $('.blockcart .product-numbers ').css({
                    'width' : '22px',
                    'height' : '22px',
                    'line-height' : '22px',
                    'border-radius' : '11px',
                    'font-size' : '14px',
                });
            } else {
                floatnav.css('position','relative');
                $('#banners').css('margin-top','auto');
                $('.logo').css({
                    'width' : '100%',
                    'max-height' : '100%',
                });
                $('#header .header-nav .search-widget').css('margin-top', '2.5rem');
                $('#header .header-nav .right-nav').css('margin-top','27px');
                $('#_desktop_user_info .login').css('width', '40px');
                $('#_desktop_cart .shopping-cart').css({
                    'width'  : '44px',
                    'hegiht' : '44px',
                });
                $('.header-contact .email').css('width', '45px');
                $('.header-contact .phone').css('width', '45px');
                $('.blockcart .product-numbers ').css({
                    'width' : '24px',
                    'height' : '24px',
                    'line-height' : '24px',
                    'border-radius' : '12px',
                    'font-size' : '16px',
                });
            }
        }
    });
});