<section id="content" class="page-content page-not-found">
    {block name='page_content'}
        <p>brak wyników wyszukiwania <span class="orange">"{$search_string}"</span></p>
        <p class="orange">Spróbuj innej frazy</p>
        <img src="{$urls.img_url}maskotka.svg" class="maskotka margin-top-20" />
    {/block}
</section>