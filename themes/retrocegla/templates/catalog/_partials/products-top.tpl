{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<div id="js-product-list-top" class="row products-selection">
    <div class="col-lg-3 col-md-6 col-xs-12">
        {block name='sort_by'}
            {include file='catalog/_partials/sort-orders.tpl' sort_orders=$listing.sort_orders}
        {/block}
    </div>
    <div class="col-lg-3 col-md-6 col-xs-12">
        <span class="col-md-12 label">Dostawa</span>
        <a href="/content/6-dostawa" class="btn btn-retro dostawa">Sprawdź ceny<img src="{$urls.img_url}dostawa.svg" alt="accept"></a>
    </div>
    <div class="col-lg-3 col-md-6 col-xs-12">
        <span class="col-md-12 label">Razem w koszyku</span>
        <button class="btn btn-retro total-price">{$cart.totals.total.amount+$cart.subtotals.tax.amount} {$currency.sign}<img src="{$urls.img_url}razem-w-koszyku.svg" alt="cart"></button>
    </div>
    <div class="col-lg-3 col-md-6 col-xs-12">
        <span class="col-md-12 label">Wybrałeś produkty?</span>
        <div class="row">
            <div class="col-md-12">
                <a href="{$urls.pages.cart}" class="btn btn-retro fajka">{l s='Proceed to checkout' d='Shop.Theme.Actions'}<img src="{$urls.img_url}fajka.svg" alt="accept"></a>
            </div>
        </div>
    </div>
</div>
