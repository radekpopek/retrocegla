{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file=$layout}

{block name='head_seo' prepend}
    <link rel="canonical" href="{$product.canonical_url}">
{/block}

{block name='head' append}
      <meta property="og:type" content="product">
      <meta property="og:url" content="{$urls.current_url}">
      <meta property="og:title" content="{$page.meta.title}">
      <meta property="og:site_name" content="{$shop.name}">
      <meta property="og:description" content="{$page.meta.description}">
      <meta property="og:image" content="{$product.cover.large.url}">
      <meta property="product:pretax_price:amount" content="{$product.price_tax_exc}">
      <meta property="product:pretax_price:currency" content="{$currency.iso_code}">
      <meta property="product:price:amount" content="{$product.price_amount}">
      <meta property="product:price:currency" content="{$currency.iso_code}">
      {if isset($product.weight) && ($product.weight != 0)}
          <meta property="product:weight:value" content="{$product.weight}">
          <meta property="product:weight:units" content="{$product.weight_unit}">
      {/if}
{/block}

{block name='content'}
    {if isset($children)}
        <!-- Subcategories -->
        <div class="subcategory-content">
            <ul>
                <li class="hidden-md-up"><img class="swipe" src="{$urls.img_url}swipe.svg" alt="swipe"/></li>
                {foreach from=$children item=subcategory}
                    <li>
                        <a class="subcategory-name {if $product.category == $subcategory.link_rewrite}active{/if}" href="{$link->getCategoryLink($subcategory.id_category, $subcategory.link_rewrite)|escape:'html':'UTF-8'}">{$subcategory.name|truncate:25:'...'|escape:'html':'UTF-8'}</a>
                    </li>
                {/foreach}
            </ul>
        </div>
    {/if}
<div id="product">
  <section id="main" itemscope itemtype="https://schema.org/Product">
    <meta itemprop="url" content="{$product.url}">

    <div class="row">
      <div class="col-md-6">
        {block name='page_content_container'}
          <section class="page-content" id="content">
            {block name='page_content'}
              {block name='product_flags'}
                <ul class="product-flags">
                  {foreach from=$product.flags item=flag}
                    <li class="product-flag {$flag.type}">{$flag.label}</li>
                  {/foreach}
                </ul>
              {/block}

              {block name='product_cover_thumbnails'}
                {include file='catalog/_partials/product-cover-thumbnails.tpl'}
              {/block}
              <div class="scroll-box-arrows">
                <i class="material-icons left">&#xE314;</i>
                <i class="material-icons right">&#xE315;</i>
              </div>

            {/block}
          </section>
        {/block}
        <div class="row">
            <div class="col-md-6 margin-top-20-sm">
                <div class="box">
                    <div class="header">
                        <button type="button" data-toggle="collapse" data-target="#box-sending">koszt dostawy <i class="material-icons">expand_more</i></button>
                    </div>
                    {if $product.id_category_default|in_array:[10,11,12,13,20]}
                        <div id="box-sending" class="collapse">
                            <div class="subbox">
                                <p>1 - 6 m<sup>2</sup> - 129 zł</p>
                            </div>
                            <div class="subbox">
                                <p>7 - 17 m<sup>2</sup> - 179 zł</p>
                            </div>
                            <div class="subbox">
                                <p>18 - 22 m<sup>2</sup> - 199 zł</p>
                            </div>
                            <p class="text">Ceny wysyłek podano w PLN brutto. Zastrzegamy sobie możliwość zmiany cen. Przesyłki i palety są ubezpieczone do rzeczywistej wartości. Klient jest zobowiązany sprawdzić zawartość przesyłki w obecności kuriera i w razie uszkodzeń spisać protokół. Paleta jest dostarczana pod dany adres (nie jest wnoszona do domu / mieszkania).</p>
                        </div>
                    {elseif $product.id_category_default|in_array:[14,15,16,17,18,19]}
                        <div id="box-sending" class="collapse">
                            <div class="subbox">
                                <p>0 - 300 kg - 129 zł</p>
                            </div>
                            <div class="subbox">
                                <p>7 - 17 m<sup>2</sup> - 179 zł</p>
                            </div>
                            <div class="subbox">
                                <p>18 - 22 m<sup>2</sup> - 199 zł</p>
                            </div>
                            <div class="subbox">
                                <p>mała paczka do 30 kg: 25 zł</p>
                            </div>
                            <p class="text">Ceny wysyłek kurierskich podano w PLN brutto. Zastrzegamy sobie możliwość zmiany cen. Przesyłki i palety są ubezpieczone do rzeczywistej wartości. Klient jest zobowiązany sprawdzić zawartość przesyłki w obecności kuriera i w razie uszkodzeń spisać protokół. Paleta jest dostarczana pod dany adres (nie jest wnoszona do domu / mieszkania).</p>
                        </div>
                    {else}
                        <div id="box-sending" class="collapse">
                            <div class="subbox">
                                <p>1 - 6 m<sup>2</sup> - 129 zł</p>
                            </div>
                            <div class="subbox">
                                <p>7 - 17 m<sup>2</sup> - 179 zł</p>
                            </div>
                            <div class="subbox">
                                <p>18 - 22 m<sup>2</sup> - 199 zł</p>
                            </div>
                            <div class="subbox">
                                <p>mała paczka do 30 kg: 25 zł</p>
                            </div>
                            <p class="text">Ceny wysyłek kurierskich podano w PLN brutto. Zastrzegamy sobie możliwość zmiany cen. Przesyłki i palety są ubezpieczone do rzeczywistej wartości. Klient jest zobowiązany sprawdzić zawartość przesyłki w obecności kuriera i w razie uszkodzeń spisać protokół. Paleta jest dostarczana pod dany adres (nie jest wnoszona do domu / mieszkania).</p>
                        </div>
                    {/if}
                </div>
            </div>
            <div class="col-md-6 margin-top-20-sm">
                <div class="box">
                    <div class="header">
                        <button data-toggle="collapse" data-target="#box-time">czas wysyłki <i class="material-icons">expand_more</i></button>
                    </div>
                    <div id="box-time" class="collapse">
                        <div class="subbox">
                            <p>od 3 do 21 dni</p>
                        </div>
                        <p class="text">Prawdziwa RetroCegła cieszy się olbrzymią popularnością wśród klientów, dlatego zdarza się, że musimy wyprodukować płytki specjalnie pod Twoje zamówienie. Staranna selekcja cegieł i ich profesjonalna obróbka, zajmuje kilka dni więcej o czym zostaniesz poinformowany.</p>
                    </div>
                </div>
            </div>
        </div>
      </div>
        <div class="col-md-6">
          {block name='page_header_container'}
            {block name='page_header'}
              <h1 class="h1 margin-top-20-sm" itemprop="name">{block name='page_title'}{$product.name}{/block}</h1>
            {/block}
          {/block}
          {block name='product_prices'}
            {include file='catalog/_partials/product-prices.tpl'}
          {/block}

          <div class="product-information">
            {block name='product_description_short'}
              <div id="product-description-short-{$product.id}" itemprop="description">{$product.description_short nofilter}</div>
            {/block}

            {if $product.is_customizable && count($product.customizations.fields)}
              {block name='product_customization'}
                {include file="catalog/_partials/product-customization.tpl" customizations=$product.customizations}
              {/block}
            {/if}

            <div class="product-actions">
              {block name='product_buy'}
                <form action="{$urls.pages.cart}" method="post" id="add-to-cart-or-refresh">
                  <input type="hidden" name="token" value="{$static_token}">
                  <input type="hidden" name="id_product" value="{$product.id}" id="product_page_product_id">
                  <input type="hidden" name="id_customization" value="{$product.id_customization}" id="product_customization_id">

                  {block name='product_variants'}
                    {include file='catalog/_partials/product-variants.tpl'}
                  {/block}

                  {block name='product_pack'}
                    {if $packItems}
                      <section class="product-pack">
                        <h3 class="h4">{l s='This pack contains' d='Shop.Theme.Catalog'}</h3>
                        {foreach from=$packItems item="product_pack"}
                          {block name='product_miniature'}
                            {include file='catalog/_partials/miniatures/pack-product.tpl' product=$product_pack}
                          {/block}
                        {/foreach}
                    </section>
                    {/if}
                  {/block}

                  {block name='product_discounts'}
                    {include file='catalog/_partials/product-discounts.tpl'}
                  {/block}
                    {block name='product_description'}
                        <div class="product-description">{$product.description nofilter}</div>
                    {/block}
                    {block name='product_details'}
                        {include file='catalog/_partials/product-details.tpl'}
                    {/block}
                  {block name='product_add_to_cart'}
                    {include file='catalog/_partials/product-add-to-cart.tpl'}
                  {/block}

                  {block name='product_additional_info'}
                    {include file='catalog/_partials/product-additional-info.tpl'}
                  {/block}

                  {block name='product_refresh'}
                    <input class="product-refresh ps-hidden-by-js" name="refresh" type="submit" value="{l s='Refresh' d='Shop.Theme.Actions'}">
                  {/block}
                </form>
              {/block}

            </div>
        </div>
      </div>
    </div>

    {block name='product_accessories'}
      {if $accessories}
        <section class="product-accessories clearfix">
          <h3>{l s='You might also like' d='Shop.Theme.Catalog'}</h3>
          <div class="products">
              <div class="bxslider">
                  {foreach from=$accessories item="product_accessory"}
                      <div>
                          {block name='product_miniature'}
                              {include file='catalog/_partials/miniatures/product.tpl' product=$product_accessory}
                          {/block}
                      </div>
                  {/foreach}
              </div>
          </div>
        </section>
      {/if}
    {/block}
    <img src="{$urls.img_url}autoryzowany montaz-pod-produktem-cena.jpg" class="image-underproduct"/>
    {block name='product_footer'}
      {hook h='displayFooterProduct' product=$product category=$category}
    {/block}

    {block name='product_images_modal'}
      {include file='catalog/_partials/product-images-modal.tpl'}
    {/block}

    {block name='page_footer_container'}
      <footer class="page-footer">
        {block name='page_footer'}
          <!-- Footer content -->
        {/block}
      </footer>
    {/block}
  </section>
</div>
{/block}
