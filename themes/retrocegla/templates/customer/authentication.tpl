{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='page.tpl'}

{block name='page_title'}
  {l s='Log in to your account' d='Shop.Theme.Customeraccount'}
{/block}

{block name='page_content'}
    <h1 class="login-header">ZALOGUJ LUB ZAREJESTRUJ SIĘ</h1>
    <hr class="light-gray margin-top-10" />
    <div class="row" style="max-width: 670px; margin-left: auto; margin-right: auto">
      {block name='login_form_container'}
          <div class="col-lg-6 col-md-12">
              <div class="no-account">
                  <h2 class="text-left">Stwórz konto</h2>
                  <p class="text-left">Podaj swój adres e-mail aby utworzyć konto.</p>
                  <form action="{$urls.pages.register}" method="post" >
                      <section class="form-fields">
                          <div class="form-group center-email-fields">
                            <input placeholder="{l s='Email address' d='Shop.Forms.Labels'}" type="email" name="email" id="email" value="{if isset($smarty.post.email)}{$smarty.post.email|stripslashes}{/if}" class="form-control" required>
                          </div>
                          <button class="form-control-submit btn btn-primary" type="submit" name="submit">
                              stwórz konto
                          </button>
                      </section>
                  </form>
              </div>
          </div>
          <div class="col-lg-6 col-md-12">
              <section class="login-form">
                  {render file='customer/_partials/login-form.tpl' ui=$login_form}
              </section>
              {block name='display_after_login_form'}
                  {hook h='displayCustomerLoginFormAfter'}
              {/block}
          </div>
      {/block}
    </div>
{/block}
