{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='customer/page.tpl'}

{block name='page_title'}
  {l s='Order history' d='Shop.Theme.Customeraccount'}
{/block}

{block name='page_content'}
  <div class="row">
    <div class="col-md-12">
      <h1>Historia i zamówienia</h1>
      <hr class="grey" />
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <p class="text-center orange font16">Poniżej wyświetlone są wszystkie Twoje zamówienia.</p>
      <hr class="orange_line" />
    </div>
  </div>

  {if $orders}
    {foreach from=$orders item=order}
      <div class="row">
        <div class="order col-md-12">
          <div class="order-row">
            <p><span class="order-header">{l s='Order reference' d='Shop.Theme.Checkout'} :</span> {$order.details.reference}</p>
          </div>
          <div class="order-row">
            <p><span class="order-header">{l s='Date' d='Shop.Theme.Checkout'} :</span> {$order.details.order_date}</p>
          </div>
          <div class="order-row">
            <p><span class="order-header">Kwota :</span> {$order.totals.total.value}</p>
          </div>
          <div class="order-row last-row">
            <p><span class="order-header">Stan zamówienia :</span>  {$order.history.current.ostate_name}</p>
          </div>
          <button type="button" class="show-more" data-toggle="collapse" data-target="#show-more_{$order.details.reference}">
            Szczegóły <i class="material-icons">expand_more</i>
          </button>
          <div id="show-more_{$order.details.reference}" class="collapse">
            {include file='customer/_partials/order-detail.tpl'}
          </div>
        </div>
      </div>
    {/foreach}
  {/if}
{/block}
{block name='page_footer'}
{/block}