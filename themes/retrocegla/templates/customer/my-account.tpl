{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='customer/page.tpl'}

{block name='page_title'}
  {l s='Your account' d='Shop.Theme.Customeraccount'}
{/block}

{block name='page_content'}
  <div class="row">
    <div class="col-md-12">
      <h1>Moje konto</h1>
      <hr class="grey" />
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <p class="text-center orange font16">Zarządzaj swoimi danymi personalnymi i przeglądaj zamówienia.</p>
      <hr class="orange_line" />
    </div>
  </div>
  <div class="row">
    <div class="links">
      {if !$configuration.is_catalog}
        <a class="col-md-6 col-sm-12 col-xs-12" id="history-link" href="{$urls.pages.history}">
          <span class="link-item">
            <span class="img-content">
              <img src="{$urls.img_url}cart_grey.svg" alt="Historia i zamówienia">
            </span>
            Historia i zamówienia
          </span>
        </a>
      {/if}
      <a class="col-md-6 col-sm-12 col-xs-12" id="identity-link" href="{$urls.pages.identity}">
        <span class="link-item">
          <span class="img-content">
            <img src="{$urls.img_url}avatar.svg" alt="Historia i zamówienia">
          </span>
          Edytuj swoje dane
        </span>
      </a>
    </div>
  </div>
  <div class="row">
    <div class="links">
      {if $configuration.voucher_enabled && !$configuration.is_catalog}
        <a class="col-md-6 col-sm-12 col-xs-12" id="discounts-link" href="{$urls.pages.discount}">
          <span class="link-item">
            <span class="img-content">
              <img src="{$urls.img_url}kupony.svg" alt="Twoje kupony rabatowe">
            </span>
            Twoje kupony rabatowe
          </span>
        </a>
      {/if}

      <a class="col-md-6 col-sm-12 col-xs-12" id="addresses-link" href="{$urls.pages.addresses}">
        <span class="link-item">
          <span class="img-content">
            <img src="{$urls.img_url}edytuj-adresy.svg" alt="Edytuj adresy">
          </span>
          Edytuj adresy
        </span>
      </a>
      {if $configuration.return_enabled && !$configuration.is_catalog}
        <a class="col-md-6 col-sm-6 col-xs-12" id="returns-link" href="{$urls.pages.order_follow}">
          <span class="link-item">
            <i class="material-icons">&#xE860;</i>
            {l s='Merchandise returns' d='Shop.Theme.Customeraccount'}
          </span>
        </a>
      {/if}

      {block name='display_customer_account'}
        {hook h='displayCustomerAccount'}
      {/block}

    </div>
  </div>
{/block}


{block name='page_footer'}
  {block name='my_account_links'}
    <div class="text-sm-center">
    </div>
  {/block}
{/block}
