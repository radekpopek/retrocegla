{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='customer/page.tpl'}

{block name='page_title'}
  {l s='Your addresses' d='Shop.Theme.Customeraccount'}
{/block}

{block name='page_content'}
  <h1>Edytuj Adresy</h1>
  <hr class="grey">
  <p class="orange">Skonfiguruj proszę swoje domyślne adresy dla rozliczeń i dostaw podczas składania zamówienia. Możesz także dodać inne adresy, co może być użyteczne przy wysyłaniu prezentów lub odbieraniu zamówienia w miejscu pracy.</p>
  <hr class="orange_line" />
  <div id="addresses-list">
    <p class="text-center bold">Poniżej znajdują się Twoje adresy.</p>
    <p class="text-center">Upewnij się, że są aktualne gdyby uległy zmianie.</p>
    {foreach $customer.addresses as $address}
      <div class="col-md-6 col-sm-12">
        {block name='customer_address'}
          {include file='customer/_partials/block-address.tpl' address=$address}
        {/block}
      </div>
    {/foreach}
    <div class="clearfix"></div>
    <div class="addresses-footer">
      <a href="{$urls.pages.address}" data-link-action="add-address" class="btn btn-success add-address">
        <span>{l s='Create new address' d='Shop.Theme.Actions'}</span>
      </a>
    </div>
  </div>
{/block}
{block name='page_footer'}{/block}
