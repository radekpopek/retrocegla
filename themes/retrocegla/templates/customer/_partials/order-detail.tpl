<div class="order-detail">
  <div class="row">
    <div class="col-md-4">
      <a href="{$order.details.reorder_url}" class="button-primary button-reorder">{l s='Reorder' d='Shop.Theme.Actions'}</a>
      <div class="paymentcarrier">
        <p>Dostawa: {$order.carrier.name} <br />
          Metoda płatności: {$order.details.payment}</p>
      </div>
      <section id="order-history" class="box">
        <h3>Stany zamówienia</h3>
        <hr class="grey" />
        {foreach from=$order.history item=state}
            <div class="box-row">
              <img alt="dzwoneczek" src="{$urls.img_url}dzwonek.svg" class="bell" />
              <div class="box-data">
                <p class="box-date">data: {$state.history_date}</p>
                <p class="box-info">{$state.ostate_name}</p>
              </div>
              <div class="clearfix"></div>
            </div>
        {/foreach}
      </section>
      <h3>Szczegóły zamówienia</h3>
      <hr class="grey" />
      <div class="order-details">
          <p>Data zamówienia: {$order.details.order_date}</p>
          <p>Stan: <span class="red">{$order.history.current.ostate_name}</span></p>
      </div>
      <div class="addresses">
          {if $order.addresses.delivery}
            <article id="delivery-address" class="box">
                <h4>{l s='Delivery address %alias%' d='Shop.Theme.Checkout' sprintf=['%alias%' => $order.addresses.delivery.alias]}</h4>
                <address>{$order.addresses.delivery.formatted nofilter}</address>
            </article>
          {/if}
          <article id="invoice-address" class="box">
              <h4>{l s='Invoice address %alias%' d='Shop.Theme.Checkout' sprintf=['%alias%' => $order.addresses.invoice.alias]}</h4>
              <address>{$order.addresses.invoice.formatted nofilter}</address>
          </article>
          <div class="clearfix"></div>
      </div>
      <div class="order-total-background">
        <div class="order-total-content">
          {foreach $order.subtotals as $line}
            {if $line.value}
              <p>{if $line.type == 'products'}Razem(brutto){elseif $line.type == 'shipping'}Dostawa{/if} : <span class="orange float-right">{$line.value}</span></p>
            {/if}
          {/foreach}
          <hr class="grey" />
          <div class="cart-total">
            <p class="text-center">Należność : <span class="green">{$order.totals.total.value}</span></p>
          </div>
        </div>
      </div>
    </div>
    <div id="your-shopping" class="col-md-8">
      <h1>Twoje zakupy</h1>
      <hr class="grey" />
      {if $order.details.is_returnable}
        {include file='customer/_partials/order-detail-return.tpl'}
      {else}
        {include file='customer/_partials/order-detail-no-return.tpl'}
      {/if}
    </div>
  </div>
  {if $order.follow_up}
    <div class="box">
      <p>{l s='Click the following link to track the delivery of your order' d='Shop.Theme.Customeraccount'}</p>
      <a href="{$order.follow_up}">{$order.follow_up}</a>
    </div>
  {/if}

    {if $order.shipping}
      <div class="box">
        <table class="table table-striped table-bordered hidden-sm-down">
          <thead class="thead-default">
            <tr>
              <th>{l s='Date' d='Shop.Theme.Global'}</th>
              <th>{l s='Carrier' d='Shop.Theme.Checkout'}</th>
              <th>{l s='Weight' d='Shop.Theme.Checkout'}</th>
              <th>{l s='Shipping cost' d='Shop.Theme.Checkout'}</th>
              <th>{l s='Tracking number' d='Shop.Theme.Checkout'}</th>
            </tr>
          </thead>
          <tbody>
            {foreach from=$order.shipping item=line}
              <tr>
                <td>{$line.shipping_date}</td>
                <td>{$line.carrier_name}</td>
                <td>{$line.shipping_weight}</td>
                <td>{$line.shipping_cost}</td>
                <td>{$line.tracking nofilter}</td>
              </tr>
            {/foreach}
          </tbody>
        </table>
        <div class="hidden-md-up shipping-lines">
          {foreach from=$order.shipping item=line}
            <div class="shipping-line">
              <ul>
                <li>
                  <strong>{l s='Date' d='Shop.Theme.Global'}</strong> {$line.shipping_date}
                </li>
                <li>
                  <strong>{l s='Carrier' d='Shop.Theme.Checkout'}</strong> {$line.carrier_name}
                </li>
                <li>
                  <strong>{l s='Weight' d='Shop.Theme.Checkout'}</strong> {$line.shipping_weight}
                </li>
                <li>
                  <strong>{l s='Shipping cost' d='Shop.Theme.Checkout'}</strong> {$line.shipping_cost}
                </li>
                <li>
                  <strong>{l s='Tracking number' d='Shop.Theme.Checkout'}</strong> {$line.tracking nofilter}
                </li>
              </ul>
            </div>
          {/foreach}
        </div>
      </div>
    {/if}
</div>