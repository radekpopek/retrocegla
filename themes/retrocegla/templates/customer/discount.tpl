{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='customer/page.tpl'}

{block name='page_title'}
  {l s='Your vouchers' d='Shop.Theme.Customeraccount'}
{/block}

{block name='page_content'}
  <h1>TWOJE KUPONY RABATOWE</h1>
  <hr class="grey" />
  <div id="discount-codes">
    {if $cart_rules}
      {foreach from=$cart_rules item=cart_rule}
        <div class="col-md-6 col-sm-12">
          <div class="discount-code">
            <div class="discount-header">
              <p>Kod rabatowy: {$cart_rule.code}</p>
            </div>
            <div class="discount-row">
              <p class="orange">{$cart_rule.name}</p>
            </div>
            <div class="discount-row">
              <p>Ilość pozostałych użyć: <span class="orange">{$cart_rule.quantity_for_user}</span></p>
            </div>
            <div class="discount-row">
              <p>Rabat: <span class="orange">{$cart_rule.value}</span></p>
            </div>
            <div class="discount-row">
              <p>Minimalna wartość zakupu: <span class="orange">{$cart_rule.voucher_minimal}</span></p>
            </div>
            <div class="discount-row">
              <p>Ważne do:<span class="orange">{$cart_rule.voucher_date}</span></p>
            </div>
          </div>
        </div>
      {/foreach}
    {/if}
  </div>
{/block}
{block name='page_footer'}
{/block}