{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<div class="container">
    <hr class="orange_line" />
    <div id="to_products">
        {if $page.page_name == "category"}
            <a href="/probki/40-oferta-demo.html" class="btn to_products">Zamów próbki cegieł</a>
        {elseif $page.page_name == "product"}
            <button onclick="history.back()" class="btn to_products">Powrót</button>
        {elseif ($page.page_name == "history") || ($page.page_name == 'identity') || ($page.page_name == 'addresses') || ($page.page_name == 'discount')}
            <a href="{$urls.pages.my_account}" class="btn to_products">Wróć</a>
        {else}
            <a href="/10-plytki-z-lica/" class="btn to_products">Przejdź do produktów</a>
        {/if}
    </div>
</div>
<img id="gwarancja" src="{$urls.img_url}5 lat gwarancji-fioletowyv3.jpg" alt="5 lat gwarancji" class="img-fluid" />
<div id="delivery_main">
    <div class="container">
        <div class="text-box">
            <h3>Dostawa już od 25zł.</h3>
            <p>Przesyłki realizują renomowane firmy kurierskie.</p>
        </div>
        <div class="text-box">
            <h3>Zadzwoń do nas</h3>
            <a class="tel" href="tel:{$shop.phone}">{$shop.phone}</a>
        </div>
        <div class="text-box">
            <h3>Odwiedź naszą stronę</h3>
            <a href="http://www.retrocegla.pl" class="page-address" target="_blank">
                www.retrocegla.pl<i class="material-icons orange">chevron_right</i>
            </a>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div id="klauzula" class="col-md-12">
            <p>Prawdziwe, stare produkty. W zależności od dostępnej partii towaru występuje możliwość przebarwień, resztek zaprawy murarskiej, odcieni kolorystycznych, odprysków, ubytków powierzchni oraz zróżnicowanie rozmiaru. Zamówione płytki ceglane mogą być wilgotne (jest to związane z technologią ich wytwarzania). W trakcie transportu dopuszcza się możliwość spękania do 20% płytek (płytka, która pękła w trakcie przewozu nie traci swoich wartości estetycznych, bez kłopotu można ją przykleić na wybraną powierzchnię, dzięki linii pęknięcia zyska tylko na swojej autentyczności.)</p>
            <p>Ceny znajdujące się na stronach internetowych firmy RetroCegła, w korespondencji e-mailowej oraz podawane podczas rozmów telefonicznych są wartościami netto (nie zawierają podatku VAT, który należy do nich doliczyć w aktualnie obowiązującej stawce), o ile wyraźnie nie zaznaczono (napisano), że dana cena jest ceną brutto.</p>
        </div>
    </div>
</div>
<div class="img-fluid" style="position: relative">
    <img alt="kalkulator płytek" src="{$urls.img_url}kalkulator_plytekv2.jpg" class="img-fluid" style="width: 100%; height: auto;"/>
    <a href="/content/9-kalkulator-plytek" style="position: absolute; z-index: 100; top: 35%; left: 45%; width: 9%; height: 28%;"></a>
</div>
<div class="footer-container">
    <div class="container">
        <div class="row">
            {block name='hook_footer_before'}
                {hook h='displayFooterBefore'}
            {/block}
        </div>
        <div class="row">
            {block name='hook_footer'}
                {hook h='displayFooter'}
            {/block}
            <div class="show-more-content hidden-sm-down">
                <hr class="orange_line" />
                <button class="show-more" data-toggle="collapse" data-target="#more-items">Pokaż więcej</button>
            </div>
        </div>
    </div>
    <div class="row">
      {block name='hook_footer_after'}
        {hook h='displayFooterAfter'}
      {/block}
    </div>
  </div>
  <div class="container">
      <div class="row margin-top-20 after-footer">
          <div class="col-md-5 col-xs-8">
              <p>Korzystanie z serwisu oznacza akceptację <a class="grey" href="/content/10-regulamin">regulaminu</a></p>
              <a href="http:///www.prmanagement.pl" target="_blank">Realizacja sklepu: Pr Management</a>
          </div>
          <div class="col-lg-2 col-md-3 hidden-sm-down social">
              <a class="inline social-icon" target="_blank" href="https://pl-pl.facebook.com/retrocegla/">
                  <img src="{$urls.img_url}facebook.svg" alt="facebook">
              </a>
              <a class="inline social-icon" target="_blank" href="https://pl.pinterest.com/retrocegla/">
                  <img src="{$urls.img_url}pinterest.svg" alt="Pinterest">
              </a>
          </div>
          <div class="col-lg-5 col-md-4">
              <img src="{$urls.img_url}logo-stopka.svg" alt="Retro Cegła" class="footer-logo">
          </div>
      </div>
      <div class="row">
          <div class="hidden-md-up col-sm-12 col-xs-12 social">
              <a class="inline social-icon" target="_blank" href="https://pl-pl.facebook.com/retrocegla/">
                  <img src="{$urls.img_url}facebook.svg" alt="facebook">
              </a>
              <a class="inline social-icon" target="_blank" href="https://pl.pinterest.com/retrocegla/">
                  <img src="{$urls.img_url}pinterest.svg" alt="Pinterest">
              </a>
          </div>
      </div>
      <div class="row cookie-info">
          <div class="col-md-12">
              <p>Strona używa plików cookie. Korzystając ze strony wyrażasz zgodę na używanie cookie, zgodnie z aktualnymi ustawieniami Twojej przeglądarki. W każdym momencie możesz dokonać zmiany ustawień dotyczących cookie. Dowiedz się więcej o celach do jakich używamy cookies. <a href="/content/11-polityka-prywatnosci">Kliknij</a>.</p>
          </div>
      </div>
  </div>
</div>
{if !preg_match("/montaz/", $smarty.server.REQUEST_URI)}
    <div id="autoryzowany-montaz">
       <div class="container">
           <div class="row">
               <div class="col-md-12">
                   <p>Skorzystaj z autoryzowanego montażu retrocegła! <span class="montaz-sprawdz"><a href="/content/7-montaz">Sprawdź</a></span></p>
               </div>
           </div>
       </div>
    </div>
{/if}
