{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{block name='header_banner'}
  <div class="header-banner">
    {hook h='displayBanner'}
  </div>
{/block}
<div class="header-background float-nav" >
  {block name='header_nav'}
    <div class="header-nav">
      <div class="container">
        <div class="row">
          <div class="hidden-sm-down">
            <div class="col-lg-3 col-md-5 hidden-sm-down" id="_desktop_logo">
              <a href="{$urls.base_url}">
                <img class="logo img-responsive" src="{$urls.img_ps_url}logo.svg" alt="{$shop.name}">
              </a>
            </div>
            <div class="col-xl-7 col-lg-5 hidden-md-down">
              {hook h="displaySearch"}
            </div>
            <div class="col-xl-2 col-lg-4 col-md-7 right-nav">
                <div class="header-contact hidden-xl-up">
                  <a href="mailto:{$shop.email}">
                    <img class="email" src="{$urls.img_url}e-mail.svg" alt="email">
                  </a>
                  <a href="tel:{$shop.phone}">
                    <img class="phone" src="{$urls.img_url}telefon.svg" alt="telefon">
                  </a>
                </div>
                {hook h='displayNav2'}
            </div>
          </div>
          <div class="hidden-md-up text-sm-center mobile">
            <div class="top-logo" id="_mobile_logo"></div>
            <div class="clearfix"></div>
          </div>
        </div>
        <div class="row mobile-menu hidden-md-up">
          <div class="col-xs-6">
            <div id="product-mobile">
              Produkty <i class="material-icons">expand_more</i>
            </div>
          </div>
          <div class="col-xs-6">
            <div class="float-xs-right" id="menu-icon">
              <div></div>
              <div></div>
              <div></div>
            </div>
          </div>
        </div>
        <div id="mobile_top_menu_wrapper" class="row hidden-md-up" style="display:none;">
          <div class="js-top-menu mobile" id="_mobile_top_menu"></div>
          <div class="js-top-menu-bottom">
            <div id="_mobile_currency_selector"></div>
            <div id="_mobile_language_selector"></div>
            <div id="_mobile_contact_link"></div>
          </div>
        </div>
        <div class="row hidden-md-up">
          <div class="col-md-12">
            <div class="mobile-icons-block">
              <div class="header-contact hidden-xl-up">
                <img id="show_email" class="email" src="{$urls.img_url}e-mail.svg" alt="email">
                <img id="show_phone" class="phone" src="{$urls.img_url}telefon.svg" alt="telefon">
              </div>
              {hook h='displayNav2'}
            </div>
            <div id="mobile-phone" class="mobile-contact">
                <p class="center">zadzwoń do nas</p>
                <a href="tel:{$shop.phone}">
                  <img width="49" height="49" src="{$urls.img_url}mobile-telefon.svg" alt="Telefon" />
                </a>
                <a href="tel:{$shop.phone}">
                  {$shop.phone}
                </a>
                <div id="hide-phone">
                  <i class="material-icons">expand_less</i>
                </div>
            </div>
            <div id="mobile-email" class="mobile-contact">
                <p class="center">napisz e-mail</p>
                <a href="mailto:{$shop.email}">
                  <img width="49" height="49" src="{$urls.img_url}mobile-mail.svg" alt="Email">
                </a>
                <a href="mailto:{$shop.email}">{$shop.email}
                </a>
                <div id="hide-email">
                  <i class="material-icons">expand_less</i>
                </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>
  {/block}

  {block name='header_top'}
    <div class="header-top">
      <div class="container">
         <div class="row">
          <div class="col-sm-12 col-md-12 col-lg-12 col-xl-8 position-static">
            {hook h='displayTop'}
            <div class="clearfix"></div>
          </div>
           <div class="col-lg-4 fast-contact">
             <a class="d-inline" href="mailto:{$shop.email}">{$shop.email}</a>
             <a class="d-inline phone" href="tel:{$shop.phone}">tel. {$shop.phone}</a>
           </div>
        </div>
      </div>
    </div>
</div>
{hook h='displayNavFullWidth'}
  <div class="mobile-search">
    <div class="container hidden-lg-up mobile-search">
      <div class="row">
        <div class="col-md-12">
          {hook h="displaySearch"}
        </div>
      </div>
    </div>
  </div>
  <div id="banners">
    <div class="container">
      {hook h='displayBanners'}
    </div>
  </div>
  <div class="container">
    <button type="button" class="show-more" data-toggle="collapse" data-target="#aditional-banners">
      <p>Pokaż więcej promocji</p>
      <i class="material-icons">expand_more</i>
    </button>
  </div>
{/block}
